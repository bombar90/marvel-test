import React , { useEffect, useReducer } from 'react'
import { FavoritesReducer } from './context/FavoritesReducer'
import { marvelContext } from './context/marvelContext'
import { useFetch } from './hooks/useFetch'
import { AppRouter } from './routes/AppRouter'



const init = () => {
    return {
        favoritos : JSON.parse( localStorage.getItem('favoritos') ) || []
    }  
}

export const MarvelHeroesApp = () => {
    const {data:characters} = useFetch(1);
    const {data:comics} = useFetch(2);    
    const {data:stories} = useFetch(5);
    const [{favoritos}, dispatch] = useReducer(FavoritesReducer, {}, init) ;
    
    useEffect(() => {
        localStorage.setItem('favoritos' , JSON.stringify(favoritos));        
    }, [favoritos])

    return (
        <marvelContext.Provider value = { { characters : characters , comics: comics , 
            categories : ['characters' , 'comics' , 'stories'] , 
            stories: stories , favoritos , dispatch }}>
            <AppRouter />
        </marvelContext.Provider>
    )
}
