import {useState , useEffect} from 'react';
import {apiKey , ts , hash} from '../helpers/apiCredentials';
import noImage from '../assets/img/marvel-placeholder.jpg'

export const useFecthScroll = (initialState , uri) => {
    const [items, setItems] = useState( initialState );
    const [offSet, setoffSet] = useState(20);
    const [hasMore, sethasMore] = useState(true);    
    
    useEffect(() => {    
        setItems(initialState);
        setoffSet(20);                
    }, [initialState]);

    const fetchData = async () => {
        const url = `${encodeURI(uri)}?apikey=${encodeURI(apiKey)}&ts=${encodeURI(ts)}&hash=${encodeURI(hash)}&limit=20&offset=${offSet}`;
        const resp = await fetch(url);
        const {data} = await resp.json();
        const {results , count} = data;        
        
        if (count > 0) {
            sethasMore(true);
            setoffSet( offSet + 20 );
        } else {
            sethasMore(false);
        }
        
        const hero = results.map(hero => {
            return {
                id: hero.id,
                name: hero.name ? hero.name : hero.title,
                description: hero.description,
                thumbnail: hero.thumbnail ? hero.thumbnail.path+'/portrait_xlarge.jpg' : noImage,
                img_grande: hero.thumbnail ?  hero.thumbnail.path+'/portrait_uncanny.jpg'  : noImage        
            }
        });        
        
        setItems(prevItems =>  {            
            return [...new Set([...prevItems , ...hero])]
        });        
    }
    
    return {
        items,
        hasMore ,
        fetchData ,
        setItems ,
        sethasMore
    }
}
