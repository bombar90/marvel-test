import { useState , useEffect } from "react"
import { getFetch } from "../helpers/getFetch"

const initialState = {
    loading : false ,
    data: []
};
export const useFetch = (tipo , id=null ) => {
    const [heroes, setHeroes] = useState(initialState);
    useEffect(() => {
        getFetch(tipo , id).then(resp => {
            setHeroes({
                loading: false ,
                data: resp
            });
        });                
    }, [tipo , id])
    return  heroes;
}
