import React from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route,        
    Redirect
  } from "react-router-dom";
import { BookmarkFav } from '../components/BookmarkFav';
import { CharactersScreen } from '../components/CharactersScreen';
import { ComicsScreen } from '../components/ComicsScreen';
import { CharactersDetail } from '../components/details/CharactersDetail';
import { ComicsDetail } from '../components/details/ComicsDetail';
import { StoriesDetail } from '../components/details/StoriesDetail';
import { HomeScreen } from '../components/HomeScreen';
import { NavBar } from '../components/layout/NavBar';
import { NotFound } from '../components/layout/NotFound';
import { StoriesScreen } from '../components/StoriesScreen';


export const AppRouter = () => {    
    return (
                
            <Router>
            <NavBar /> 
                <div className="container mt-1">
                <Switch>
                    <Route exact path="/home" component={HomeScreen} />                       
                    <Route exact path="/characters" component={CharactersScreen}  />                        
                    <Route exact path="/stories" component={StoriesScreen}  />                        
                    <Route exact path="/comics" component={ComicsScreen} />                                                              
                    <Route exact path="/bookmark" component={BookmarkFav} />                                                              
                    <Route exact path="/characters/:id" component={CharactersDetail} />                        
                    <Route exact path="/comics/:id" component={ComicsDetail} />                        
                    <Route exact path="/stories/:id" component={StoriesDetail} />
                    <Route exact path="/404" component={NotFound} />
                    <Redirect to="/home" />
                </Switch>
                </div>
            </Router>        
    )
}
