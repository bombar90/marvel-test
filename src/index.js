import React from 'react';
import ReactDOM from 'react-dom';
import { MarvelHeroesApp } from './MarvelHeroesApp';
import './styles/index.scss'

ReactDOM.render(
    <MarvelHeroesApp />,
    document.getElementById('root')
);
