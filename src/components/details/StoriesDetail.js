import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'
import { getFetch } from '../../helpers/getFetch';
import { CharacterList } from './CharacterList';
import { ComicsList } from './ComicsList';

export const StoriesDetail = ({history}) => {

    const {id} = useParams();    
    const [story, setStory] = useState({});            
    useEffect(() => {
        getFetch(6 , id).then(resp => {
            const data = resp.find( story => story.id === Number(id));            
            if (!data) {
                history.push('/404');
            } else {
                setStory(data);            
            }    
        });           
    }, [id , history]);
    const renderDescription = () => {
        return (story.description) ? <p className="text-muted" >{story.description}</p>                                    
                           : <p className="text-muted" > there is no description available </p>        
    };
    const renderComics = () => {
        return <ComicsList id={id}  category="stories" />
    } ;
    const renderChars = () => {        
           return <CharacterList id={id} category="stories"  />
    };    
    return (
        <div className="row mt-5">
            <div className="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4" >
                <img className="img-fluid animate__animated animate__fadeInDown" src={story.img_grande} alt={story.name} />
            </div>
            <div className="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-8" >
                <h3 className="mt-2">{story.name}</h3>
                <hr />
                { 
                    renderDescription()
                }
                <hr/>
                <h3>Characters</h3>                
                {
                    renderChars()
                }                
                <hr/>
                <h3>Comics</h3>                
                {
                    renderComics()
                }                
            </div>
        </div>
    )
}
