import React, { useRef } from 'react'
import { useFetchDetails } from '../../hooks/useFetchDetails';
import InfiniteScroll from 'react-infinite-scroll-component';
import { useFecthScroll } from '../../hooks/useFecthScroll';
import { useHistory } from 'react-router-dom';

export const ComicsList = ({id , category}) => {
    const history = useHistory();
    const uri = `https://gateway.marvel.com:443/v1/public/${category}/${id}/comics`;
    const {data:comics} =  useFetchDetails(uri);
    const {items , hasMore , fetchData} = useFecthScroll(comics , uri);
    const ref = useRef(null);

    const handleClick = (id_comic) => {        
        history.push(`/comics/${id_comic}`);
    };


    const render = () => {
        if(items.length > 0) {
            return <div id="scroll-comic" ref={ref} className="row scroll__scroll-detail">
                    <InfiniteScroll
                        dataLength={items.length} 
                        next={fetchData}
                        hasMore={hasMore}
                        loader={ <h4>Loading...</h4>} 
                        scrollableTarget="scroll-comic"                
                    >
                    {          
                        items.map((comic , index) => (
                            <div className="col-3 mb-2 animate__animated animate__fadeInUp pointer" key={`${comic.id}-${index}`}  onClick={ () => handleClick(comic.id) } >
                                <img className="img-thumbnail" src={comic.thumbnail} alt={comic.name} />                            
                                <div><small className="text-muted"> { comic.name} </small></div>
                            </div>
                        )) 
                    }
                    </InfiniteScroll>
                </div>
        } else {
            return <p className="text-muted" > No Comics available</p>
        }
    }
    return (
        render()
    )
}
