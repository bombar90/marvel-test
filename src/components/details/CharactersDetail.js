import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'
import { getFetch } from '../../helpers/getFetch';
import { ComicsList } from './ComicsList';
import { StoriesList } from './StoriesList';
import noImage from '../../assets/img/marvel-placeholder.jpg'

export const CharactersDetail = ( {history} ) => {
    const {id} = useParams();
    const [char, setchar] = useState({});            
    useEffect(() => {
        getFetch(3 , id).then(resp => {
            const data = resp.find( char => char.id === Number(id));            
            if (!data) {
                history.push('/404');
            } else {
                setchar(data);            
            }
        });            
    }, [id , history]);
    const renderDescription = () => {
        return (char.description) ? <p className="text-muted" >{char.description}</p>                                    
                           : <p className="text-muted" > there is no description available </p>        
    };
    const renderComics = () => {
        return <ComicsList id={id}  category="characters" />
    } ;
    const renderStories = () => {        
           return <StoriesList id={id} category="characters"  />
    };
    const addDefaultSrc = (ev) => {
        ev.target.src = noImage
    }
    return (
        <div className="row no-gutters mt-5">
            <div className="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 text-center" >
                <img className="img-thumbnail animate__animated animate__fadeInDown" onError={addDefaultSrc} src={char.img_grande} alt={char.name} />
            </div>
            <div className="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-8" >
                <h3>{char.name}</h3>
                <hr />
                {  renderDescription() }
                <hr/>
                <h3>Comics</h3>
                    {renderComics()}                
                <hr/>
                <h3>Stories</h3>                
                {  renderStories() }                
            </div>
        </div>
    )
}
