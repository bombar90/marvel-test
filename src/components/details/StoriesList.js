import React , {useRef} from 'react'
import { useFetchDetails } from '../../hooks/useFetchDetails';
import { useFecthScroll } from '../../hooks/useFecthScroll';
import InfiniteScroll from 'react-infinite-scroll-component';
import { useHistory } from 'react-router-dom';

export const StoriesList = ({id , category}) => {
    const uri = `https://gateway.marvel.com:443/v1/public/${category}/${id}/stories`;
    const history = useHistory();
    const {data:stories} =  useFetchDetails(uri);
    const {items , hasMore , fetchData} = useFecthScroll(stories , uri);
    const ref = useRef(null);

    const handleClick = (id_story) => {        
        history.push(`/stories/${id_story}`);
    };

    const render = () => {        
        if (items.length > 0) {
            return  <div id="scroll-story" ref={ref} className="row scroll__scroll-detail">
                <InfiniteScroll
                    dataLength={items.length} 
                    next={fetchData}
                    hasMore={hasMore}
                    loader={ <h4>Loading...</h4>} 
                    scrollableTarget="scroll-story"               
                >
                    {                
                        items.map(story => (
                            <div className="col-3 mb-2 animate__animated animate__fadeInUp pointer" onClick={ () => handleClick(story.id) } key={story.id} >
                                <img className="img-thumbnail" src={story.thumbnail } alt={story.name} />                                
                                <div> <small className="text-muted"> {story.name} </small> </div>
                            </div>
                        ))
                    }
                </InfiniteScroll>
            </div>
        } else  {
            return <p className="text-muted" > No Stories available</p>
        }
    };
    return (        
            render()        
    )
}
