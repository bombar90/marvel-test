import React , {useRef} from 'react'
import { useFetchDetails } from '../../hooks/useFetchDetails';
import InfiniteScroll from 'react-infinite-scroll-component';
import { useFecthScroll } from '../../hooks/useFecthScroll';
import { useHistory } from 'react-router-dom';


export const CharacterList = ({id , category}) => {
    const uri = `https://gateway.marvel.com:443/v1/public/${category}/${id}/characters`;
    const {data:char} =  useFetchDetails(uri);
    const {items , hasMore , fetchData} = useFecthScroll(char , uri);
    const ref = useRef(null);
    const history = useHistory();
    const handleClick =(charId) => {
        history.push(`/characters/${charId}`);
    }

    const render = () => {        
        if (char.length > 0) {            
            return  <div id="scroll-char" ref={ref} className="row scroll__scroll-detail">
                <InfiniteScroll
                    dataLength={items.length} 
                    next={fetchData}
                    hasMore={hasMore}
                    loader={ <h4>Loading...</h4>} 
                    scrollableTarget="scroll-char"               
                >
                    {                
                        items.map(char => (
                            <div className="col-3 mb-2 animate__animated animate__fadeInUp pointer" onClick={ () => handleClick(char.id)}  key={char.id} >
                                <img className="img-thumbnail" src={char.thumbnail } alt={char.name} />                                
                                <div>
                                    <small className="text-muted"> {char.name} </small>
                                </div>
                            </div>
                        ))
                    }
                </InfiniteScroll>
            </div>
        } else  {
            return <p className="text-muted" > No characters availables</p>
        }
    };
    return (        
            render()        
    )
}