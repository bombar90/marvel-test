import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom';
import { getFetch } from '../../helpers/getFetch';
import { CharacterList } from './CharacterList';
import { StoriesList } from './StoriesList';

export const ComicsDetail = ({history}) => {
    const {id} = useParams();    
    const [comic, setComic] = useState({});            
    useEffect(() => {
        getFetch(4 , id).then(resp => {
            const data = resp.find( comic => comic.id === Number(id));
            if (!data) {
                history.push('/404');
            } else {
                setComic(data);            
            }            
        });            
    }, [id  , history]);
    const renderDescription = () => {
        return (comic.description) ? <p className="text-muted" >{comic.description}</p>                                    
                           : <p className="text-muted" > there is no description available </p>        
    };
    return (
        <div className="row mt-5">
            <div className="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4" >
                <img className="img-fluid" src={comic.img_grande} alt={comic.name} />
            </div>
            <div className="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-8" >
                <h3>{comic.name}</h3>
                <hr />
                { 
                    renderDescription()
                }
                <hr />
                <h3>Characters Comic</h3>
                    <CharacterList id= {id} category="comics"  />            
                <hr />
                <h3>Stories</h3>
                    <StoriesList id={id} category ="comics"  />            
            </div>
        </div>
    )
}
