import React, { useContext } from 'react'
import { marvelContext } from '../context/marvelContext';
import { BannerComponent } from './layout/BannerComponent';
import { MarvelList } from './layout/MarvelList'

export const ComicsScreen = () => {
    const {comics , categories} = useContext(marvelContext);
    const [, category] = categories;
    return (
        <div>
            <BannerComponent />
            <h2 className="mt-5 text-center">Marvel Comics</h2>
            <hr />
            <MarvelList category= {category}  data = {comics}   />
        </div>
    )
}
 