import React , {Fragment} from 'react'
import banner from '../assets/img/banner.jpg'
import banner2 from '../assets/img/banner2.jpeg'
import comic1 from '../assets/img/comic1.jpg'

export const HomeScreen = () => {
    return (

        <Fragment>
            <div className="row mt-3">
                <div className="col-12 text-center animate__animated animate__fadeIn" >
                    <h1>Welcome to your Marvels Page</h1>
                    <h4>Here you can filter , search and get details of your favorites marvel characters!</h4>
                </div>
            </div>
            <div className="row no-gutters p-5">
                <div className="col-8 p-0">
                    <img  className="img-fluid w-100 animate__animated animate__fadeInUp" src={banner2} alt="banner2"/>
                    <img  className="img-fluid w-100 animate__animated animate__fadeInBottomLeft" src={banner} alt="banner"/>
                </div>
                <div className="col-4 p-0">
                    <img className="img-fluid animate__animated animate__fadeInRight h-100" src={comic1} alt="banner"/>
                </div>
            </div>
        </Fragment>

    )
}
