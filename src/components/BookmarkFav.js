import React, { useContext } from 'react'
import { marvelContext } from '../context/marvelContext';
import { BannerComponent } from './layout/BannerComponent'
import { MarvelCard } from './layout/MarvelCard';

export const BookmarkFav = () => {
    const {favoritos} = useContext(marvelContext)
    const renderCards = () => {
            if (favoritos.length > 0) {
                return favoritos.map( (item  , index) => (
                    <MarvelCard key={ `${item.id}-${index}` } {...item} />
                ))
            } else {
                <p>No hay data</p>
            }         
         } ;

    return (
        <div>
            <BannerComponent />
            <h2 className="mt-5 text-center"> Favorites </h2>
            <hr />

            <div className="row">
                {
                    renderCards()
                }
            </div>


        </div>
    )
}
