import React, { useContext } from 'react'
import { marvelContext } from '../context/marvelContext';
import { BannerComponent } from './layout/BannerComponent';
import { MarvelList } from './layout/MarvelList';

export const CharactersScreen = () => {
    const {characters , categories } = useContext(marvelContext);
    const [category] = categories;
    return (
            <div>
                <BannerComponent />
                <h2 className="mt-5 text-center" >Marvel Characters</h2>        
                <hr />                
                <MarvelList  category= {category} data = {characters} />
            </div>    
    )
}
 