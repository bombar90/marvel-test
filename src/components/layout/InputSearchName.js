import React from 'react'
import { getSearchName } from '../../helpers/getSearchName';
import { useForm } from '../../hooks/useForm';

export const InputSearchName = ( {setItems , sethasMore , uri , data } ) => {
    const [values , handleInputChange , reset]  = useForm({ search : '' });
    const {search} =  values;
    const handleSubmit = async ( e ) => {
        e.preventDefault();        
        if (search) {
            const searchItems = await getSearchName(uri , search);
            console.log(searchItems);
            setItems(searchItems);
            sethasMore(false);
        }
    };
    const handleReset = () => {
        reset({ search : '' });
        sethasMore(true);
        setItems(data);
    };
    return (
        <form onSubmit={handleSubmit}>
            <div className="input-group">
                <input type="text"  className="form-control" 
                        autoComplete="off" placeholder= {`Search your Character`}  
                        value={search}  name="search" onChange={handleInputChange}      
                />
                <div className="input-group-append">
                    <span className="input-group-text" id="basic-addon2">
                        <button type="button" className="btn btn-danger" onClick={handleReset}  >
                            <i className="fas fa-redo-alt"></i>
                        </button>
                    </span>
                </div>
            </div>
        </form>
    )
}
