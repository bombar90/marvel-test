import React, { useEffect } from 'react'
import { getSearchComic } from '../../helpers/getSearchComic';
import { useForm } from '../../hooks/useForm';

export const InputSearchComics = ({setItems , sethasMore , uri , data }) => {
    const [values , handleInputChange , reset]  = useForm({ search : '' , filterType: 'titleStartsWith' });
    const {search , filterType} =  values;

    useEffect(() => {
        if (filterType === 'format') {            
        }         
    }, [filterType])
    const handleSubmit = async ( e ) => {
        e.preventDefault();
        console.log(search , filterType);        
        if (search) {
            const searchItems = await getSearchComic(uri , search , filterType);            
            setItems(searchItems);
            sethasMore(false);
        }
    };
    const handleReset = () => {
        reset({ search : '' , filterType: 'titleStartsWith' });
        sethasMore(true);
        setItems(data);
    };
    return (
        <div>
             <form onSubmit={handleSubmit}>
                    <div className="input-group">
                        <input type="text"  className="form-control" 
                                autoComplete="off" placeholder= "Search your comic"  
                                value={search}  name="search" onChange={handleInputChange}      
                        />
                        <div className="input-group-append">
                            <span className="input-group-text" id="basic-addon2">
                                <select className="form-select" aria-label="Default select example" onChange={handleInputChange} name="filterType" value ={filterType} >                                    
                                    <option value="titleStartsWith">Title</option>
                                    <option value="format">Format</option>
                                    <option value="issueNumber">Issue Number</option>
                                </select>
                            </span>
                        </div>
                        <div className="input-group-append">
                            <span className="input-group-text" id="basic-addon2">
                                <button type="button" className="btn btn-danger" onClick={handleReset}  >
                                    <i className="fas fa-redo-alt"></i>
                                </button>
                            </span>
                        </div>
                    </div>
            </form>
        </div>
    )
}
