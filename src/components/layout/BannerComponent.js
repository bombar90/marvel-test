import React from 'react';
import banner from '../../assets/img/banner.jpg'

export const BannerComponent = () => {
    return (
        <div className="row mt-3">
            <div className="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 animate__animated animate__fadeInLeft">
                <div id="carouselExampleSlidesOnly" className="carousel slide" data-bs-ride="carousel">
                    <div className="carousel-inner">
                        <div className="carousel-item active">
                            <img src={banner} className="d-block w-100" alt="..." />
                        </div>                       
                    </div>
                </div>       
            </div>
        </div>
    )
}
