import React, { useRef, Fragment } from "react";
import { MarvelCard } from "./MarvelCard";
import InfiniteScroll from "react-infinite-scroll-component";
import { useFecthScroll } from "../../hooks/useFecthScroll";
import { InputSearchName } from "./InputSearchName";
import { InputSearchComics } from "./InputSearchComics";

export const MarvelList = ({ category, data }) => {
  const uri = `https://gateway.marvel.com:443/v1/public/${category}`;
  const { items, hasMore, fetchData, setItems, sethasMore } = useFecthScroll(
    data,
    uri
  );
  const ref = useRef(null);
  const renderCards = () =>
    items.map((item, index) => (
      <MarvelCard key={`${item.id}-${index}`} {...item} category={category} />
    ));
  const renderInput = () => {
    if (category === "characters") {
      return (
        <InputSearchName
          setItems={setItems}
          sethasMore={sethasMore}
          uri={uri}
          data={data}
        />
      );
    } else if (category === "comics") {
      return (
        <InputSearchComics
          setItems={setItems}
          sethasMore={sethasMore}
          uri={uri}
          data={data}
        />
      );
    }
  };

  return (
    <Fragment>
      <div className="row justify-content-center">
        <div className="col-8">{renderInput()}</div>
      </div>
      <div id="scroll" ref={ref} className="row mt-2 scroll__scroll-contanier">
        <InfiniteScroll
          dataLength={items.length}
          next={fetchData}
          hasMore={hasMore}
          loader={<h4>Loading...</h4>}
          scrollableTarget="scroll"
        >
          {renderCards()}
        </InfiniteScroll>
      </div>
    </Fragment>
  );
};
