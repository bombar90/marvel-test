
import {apiKey , ts , hash} from './apiCredentials'
import noImage from '../assets/img/marvel-placeholder.jpg'

export const getFetch = async (tipo , id=null) => {
    
    let uri = '';
    switch (tipo) {
        case 1:
            // characters
            uri =  'https://gateway.marvel.com:443/v1/public/characters'
            break;
        case 2:
            // comics
            uri =  'https://gateway.marvel.com:443/v1/public/comics'
            break;        
        case 3:
            // characters detail
            uri =  `https://gateway.marvel.com:443/v1/public/characters/${id}`
            break;        
        case 4:
            // comics detail
            uri =  `https://gateway.marvel.com:443/v1/public/comics/${id}`
            break;        
        case 5:
            // Stories
            uri =  `https://gateway.marvel.com:443/v1/public/stories`
            break;        
        case 6:
            // Stories
            uri =  `https://gateway.marvel.com:443/v1/public/stories/${id}`
            break;        
        default:
            break;
    }
    
    const url = `${encodeURI(uri)}?apikey=${encodeURI(apiKey)}&ts=${encodeURI(ts)}&hash=${encodeURI(hash)}`;
    const resp = await fetch(url);
    const {data} = await resp.json();    
    const heroes = data.results.map(hero => {
        return {
            id: hero.id,
            name: hero.name ? hero.name : hero.title,
            description: hero.description,
            thumbnail: hero.thumbnail ? hero.thumbnail.path+'/portrait_xlarge.jpg' : noImage,
            img_grande: hero.thumbnail ?  hero.thumbnail.path+'/portrait_uncanny.jpg'  : noImage      
        }
    })    
    return heroes;
}


