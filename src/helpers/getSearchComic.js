
import {apiKey , ts , hash} from './apiCredentials';
import noImage from '../assets/img/marvel-placeholder.jpg'

export const getSearchComic = async (url , search , filter) => {
    const resp = await fetch(`${encodeURI(url)}?apikey=${encodeURI(apiKey)}&ts=${encodeURI(ts)}&hash=${encodeURI(hash)}&${filter}=${search}`);
    const {data} = await resp.json();
    const heroes = data.results.map(hero => {
        return {
            id: hero.id,
            name: hero.name ? hero.name : hero.title,
            description: hero.description,
            thumbnail: hero.thumbnail ? hero.thumbnail.path+'/portrait_xlarge.jpg' : noImage,
            img_grande: hero.thumbnail ?  hero.thumbnail.path+'/portrait_uncanny.jpg'  : noImage      
        }
    })
    return heroes;
}
 