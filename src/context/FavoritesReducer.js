import { types } from "../types/types";

const initialState = {
    favoritos : []
};

export const FavoritesReducer = ( state = initialState , action ) => {

    switch (action.type) {
        case types.setFavorites:
            return {
                ...state ,                
                favoritos : [...state.favoritos , action.payload]
            }                
        case types.delFavorites:
            return {
                ...state ,
                favoritos: [...state.favoritos.filter(a=>a.id !== action.payload.id)]

              }        
        default:
            return state;
    }

    
}